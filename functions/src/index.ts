const admin = require('firebase-admin');
const functions = require('firebase-functions');

admin.initializeApp(functions.config().firebase);

const db = admin.firestore();

exports.addRouteToProfile = functions.firestore
  .document('/routes/{routeID}')
  .onCreate(async (routeSnap: any, context: any) => {
    const route = routeSnap.data();

    const profileSnap = await route.profile.get();
    const profile = profileSnap.data();
    const currentDistance = profile.totalDistance ? profile.totalDistance : 0;
    const totalDistance = currentDistance + route.distance;

    await route.profile.collection('routes').doc(context.params.routeID).set({ route: routeSnap.ref }, { merge: true });
    const actions = await db.collection('actions').where('active', '==', true).limit(1).get();

    let totalProfileScore = profile.totalScore || 0;

    actions.forEach(async (item: any) => {
      const action = item.data();
      const type = route.type || 'on_foot';
      const data = action.types[type];
      const qualified = route.distance >= data.qualifyingDistance && route.totalTimeInSeconds >= data.qualifyingTime;
      let score = 0;
      if (qualified === true) {
        score = data.score;
        totalProfileScore += score;
      }

      await db.collection('score').add({ score, type, distance: route.distance, createdAt: route.endAt, route: routeSnap.ref, profile: profileSnap.ref });
      await routeSnap.ref.set({ qualified, score }, { merge: true });
    })

    await route.profile.set({ totalDistance, totalScore: totalProfileScore }, { merge: true });
  });