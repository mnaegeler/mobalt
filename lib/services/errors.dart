const Map errors = {
  'ERROR_USER_NOT_FOUND': 'Usuário não encontrado',
  'ERROR_INVALID_EMAIL': 'O e-mail inserido é inválido.',
  'ERROR_OPERATION_NOT_ALLOWED': 'Acesso não permitido.',
  'ERROR_WRONG_PASSWORD': 'A senha inserida está incorreta ou o usuário não possui senha.',
  'ERROR_EMAIL_ALREADY_IN_USE': 'Este e-mail já está em uso.',
  'ERROR_WEAK_PASSWORD': 'A senha deve conter ao menos 6 caracteres.',
};