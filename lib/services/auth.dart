import 'package:firebase_auth/firebase_auth.dart';

class AuthData {
  static String uid;
  static FirebaseUser user;
}

class AuthService {
  FirebaseUser _user;

  final FirebaseAuth _auth = FirebaseAuth.instance;

  get user {
    return _user;
  }

  get auth {
    return _auth;
  }

  AuthService() {
    _auth.currentUser().asStream().listen((user) {
      AuthData.uid = user?.uid;
      AuthData.user = user;
      _user = user;
    });
  }

  Future<dynamic> logout() async {
    await _auth.signOut();
  }

}