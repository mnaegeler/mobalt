import 'package:flutter/material.dart';
import 'package:mobalt/colors.dart';
import 'package:mobalt/pages/profile.dart';
import 'package:mobalt/pages/ranking.dart';

import '../pages/home.dart';

Drawer buildDrawer(BuildContext context, String currentRoute) {
  return Drawer(
    child: ListView(
      children: <Widget>[
        DrawerHeader(
          child: Center(
            child: Text('MobAlt', style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold, color: greyColor),),
          ),
        ),
        ListTile(
          title: Text('Ranking'),
          leading: Icon(Icons.trending_up),
          selected: currentRoute == RankingPage.route,
          onTap: () {
            Navigator.pop(context);
            Navigator.pushNamed(context, RankingPage.route);
          },
        ),
        ListTile(
          title: Text('Home'),
          leading: Icon(Icons.home),
          selected: currentRoute == HomePage.route,
          onTap: () {
            Navigator.pop(context);
            Navigator.pushReplacementNamed(context, HomePage.route);
          },
        ),
        ListTile(
          title: Text('Perfil'),
          leading: Icon(Icons.person),
          selected: currentRoute == ProfilePage.route,
          onTap: () {
            Navigator.pop(context);
            Navigator.pushReplacementNamed(context, ProfilePage.route);
          },
        ),
      ],
    ),
  );
}
