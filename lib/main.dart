import 'package:flutter/material.dart';
import 'package:mobalt/pages/activity.dart';
import 'package:mobalt/pages/home.dart';
import 'package:mobalt/pages/profile.dart';
import 'package:mobalt/pages/ranking.dart';
import 'package:mobalt/pages/register.dart';
import 'package:mobalt/services/auth.dart';
import 'package:mobalt/colors.dart';

import './pages/auth.dart';

void main() => runApp(MyApp());

//final ThemeData _themeData = _buildTheme();
  final TextStyle font = TextStyle(fontFamily: 'Manjari');
/*
ThemeData _buildTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    accentColor: greyColor,
    primaryColor: greyColor,
    floatingActionButtonTheme: base.floatingActionButtonTheme.copyWith(
      backgroundColor: yellowColor,
      foregroundColor: Color(0xFF000000),
    ),
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor: yellowColor,
      textTheme: ButtonTextTheme.normal,
    ),
  );
}
*/

class HomePageRouter extends StatefulWidget {
  @override
  HomePageRouterState createState() => HomePageRouterState();
}

class HomePageRouterState extends State<HomePageRouter> {
  final auth = AuthService();

  HomePageRouterState() {
    auth.auth.currentUser().asStream().listen((user) {
      setState(() {
        if (auth.user != null) {
          Navigator.pushReplacementNamed(context, HomePage.route);
        } else {
          Navigator.pushReplacementNamed(context, AuthPage.route);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(color: Colors.white);
  }
}

class MyApp extends StatelessWidget {
  final String _name = "MobAlt";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _name,
      theme: ThemeData(
        fontFamily: 'Manjari',
        primaryColor: greyColor,
        floatingActionButtonTheme: FloatingActionButtonThemeData(
          backgroundColor: yellowColor,
          foregroundColor: greyColor,
        ),
      ),
      home: HomePageRouter(),
      routes: <String, WidgetBuilder>{
        HomePage.route: (context) => HomePage(),
        RankingPage.route: (context) => RankingPage(),
        AuthPage.route: (context) => AuthPage(),
        ProfilePage.route: (context) => ProfilePage(),
        RegisterPage.route: (context) => RegisterPage(),
        ActivityPage.route: (context) => ActivityPage(),
      }
    );
  }
}