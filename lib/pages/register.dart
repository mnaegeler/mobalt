import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:mobalt/pages/home.dart';
import 'package:mobalt/services/errors.dart';

const String _route = '/register';


class RegisterPage extends StatefulWidget {
  static const String route = _route;

  @override
  State createState() => RegisterPageState();
}

class RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();
  bool _processing = false;
  String _errorMessage;
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Firestore db = Firestore.instance;

  Future<void> formSubmit() async {
    if (_emailController.text.isEmpty || _passwordController.text.isEmpty) {
      return;
    }

    setState(() {
      _errorMessage = null;
      _processing = true;
    });

    String email = _emailController.text;
    String pass = _passwordController.text;


    void redirect() {
      setState(() {
        _processing = false;
      });

      _emailController.clear();
      _passwordController.clear();
      Navigator.pushReplacementNamed(context, HomePage.route);
    }

    _auth.createUserWithEmailAndPassword(email: email, password: pass)
      .then((AuthResult result) {
        _auth.signInWithEmailAndPassword(email: email, password: pass)
          .then((AuthResult result) {
            final String _userUID = result.user.uid;
            var subs;
            subs = db.document('/profiles/$_userUID').snapshots().listen((profile) {
              var data = profile.data;
              subs.cancel();
              if (data == null) {
                db.document('/profiles/$_userUID').setData({
                  'uid': _userUID,
                  'name': '',
                  'email': result.user.email,
                }, merge: true).whenComplete(redirect);
              } else {
                redirect();
              }
            });
          });
      })
      .catchError((error) {
        print(error);
        setState(() {
          _processing = false;
          _errorMessage = errors[error.code];
        });
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Criar conta'), centerTitle: true),
      body: ListView(
        padding: EdgeInsets.all(8.0),
        children: <Widget>[
          Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 16),
                  child: Text('Entre com o e-mail e senha para sua conta:', style: TextStyle(fontSize: 18),),
                  alignment: Alignment.center,
                ),

                TextFormField(
                  controller: _emailController,
                  onEditingComplete: formSubmit,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(labelText: 'E-mail'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Insira seu e-mail';
                    }
                    return null;
                  },
                ),

                TextFormField(
                  controller: _passwordController,
                  onEditingComplete: formSubmit,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(labelText: 'Senha'),
                  obscureText: true,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Insira sua senha';
                    }
                    if (value.length < 6) {
                      return 'A senha deve conter ao menos 6 caracteres';
                    }
                    return null;
                  },
                ),

                _errorMessage == null ? Container() : Column(
                  children: <Widget>[
                    Text('Houve um erro:', style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),),
                    Text(_errorMessage, style: TextStyle(color: Colors.red)),
                  ],
                ),
                Container(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: Container(
                    constraints: BoxConstraints.expand(height: 36),
                    child: FlatButton(
                      color: Colors.blue,
                      textColor: Colors.white,
                      onPressed: _processing == true ? null : () {
                        if (_formKey.currentState.validate()) {
                          formSubmit();
                        }
                      },
                      child: Text('SALVAR'),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}