import 'package:flutter/material.dart';
import 'package:mobalt/services/auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../widgets/drawer.dart';
import 'auth.dart';

const String _route = '/profile';

class ProfileService {
  final Firestore db = Firestore.instance;
  AuthService authService = AuthService();

  var _profile;
  String _userUID;

  get profile {
    return _profile;
  }

  get name {
    return _profile != null ? _profile.name : '';
  }

  saveProfile(String name) {
    return db.document('/profiles/$_userUID').setData({
      'uid': authService.user.uid,
      'name': name,
      'email': authService.user.email,
    });
  }
}

class ProfilePage extends StatefulWidget {
  static const String route = _route;

  @override
  State createState() => ProfilePageState();
}

class ProfilePageState extends State<ProfilePage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _nameController;
  String _name = '';
  bool _isEdit = false;


  ProfileService profileService = ProfileService();
  AuthService authService = AuthService();
  final Firestore db = Firestore.instance;

  bool _processing = false;

  void formSubmit() async {
    if (_nameController.text.isEmpty) {
      return;
    }

    setState(() {
      _processing = true;
    });

    String name = _nameController.text;
    final Firestore db = Firestore.instance;
    final _userUID = authService.user.uid;
    await db.document('/profiles/$_userUID').setData({
      'uid': authService.user.uid,
      'name': name,
      'email': authService.user.email,
    }, merge: true);

    setState(() {
      _processing = false;
      _isEdit = false;
    });
  }

  ProfilePageState() {
    authService.auth.currentUser().asStream().listen((user) {
      if (authService.user != null) {
        final _userUID = authService.user.uid;
        db.document('/profiles/$_userUID').snapshots().listen((profile) {
          var data = profile.data;
          _nameController = TextEditingController();
          _nameController.text = data != null ? data['name'] : '';
          if(!mounted) return;
          setState(() {
            _name = data != null ? data['name'] : '';
          });
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Meu Perfil'),
        actions: <Widget>[
          _isEdit == false ? FlatButton(
            child: Text('Sair', style: TextStyle(color: Colors.white),),
            onPressed: () async {
              await authService.logout();
              Navigator.pushReplacementNamed(context, AuthPage.route);
            },
          ) : Text(''),
          _isEdit == false ? IconButton(icon: Icon(Icons.edit), onPressed: () {
            setState(() {
              _isEdit = true;
            });
          }) : FlatButton(
            child: Text('Cancelar', style: TextStyle(color: Colors.white),),
            onPressed: () {
              setState(() {
                _isEdit = false;
              });
            },
          )
        ],
      ),
      drawer: buildDrawer(context, _route),
      body: ListView(
        padding: EdgeInsets.all(8.0),
        children: <Widget>[
          _isEdit ? Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                TextFormField(
                  controller: _nameController,
                  onEditingComplete: formSubmit,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(labelText: 'Nome'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Insira seu nome';
                    }
                    return null;
                  },
                ),

                Container(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: Container(
                    constraints: BoxConstraints.expand(height: 36),
                    child: FlatButton(
                      color: Colors.blue,
                      textColor: Colors.white,
                      onPressed: _processing == true ? null : () {
                        if (_formKey.currentState.validate()) {
                          formSubmit();
                        }
                      },
                      child: Text('SALVAR'),
                    ),
                  ),
                ),
              ],
            ),
          ) : Container(
            padding: EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text('Nome', style: TextStyle(fontSize: 14),),
                Text(_name, style: TextStyle(fontSize: 20),),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
