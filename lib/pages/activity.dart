import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mobalt/colors.dart';
import 'package:mobalt/services/profile.dart';
import 'package:intl/intl.dart';

const String _route = '/activity';

const Map _activitiesTypes = {
  'bike': 'Bicicleta',
  'on_foot': 'A pé',
};

class ActivityTypeArguments {
  static String type;
}

class Route {
  DocumentReference _uid;
  DocumentReference _profile;
  DateTime _startAt;
  DateTime _endAt;
  List<Map<String, dynamic>> _route;
  double _distance = 0;
  int _totalTimeInSeconds;
  String measure = 'M';

  final Firestore db = Firestore.instance;
  final formatDecimals = new NumberFormat('###.00', 'pt_BR');
  final formatIntegers = new NumberFormat('###', 'pt_BR');

  Route(DocumentReference profile) {
    db.settings(persistenceEnabled: true);
    _uid = db.collection('/routes').document();
    _profile = profile;
    _route = List();
    _startAt = new DateTime.now();
  }

  get length {
    return _route.length;
  }

  get last {
    return _route.length > 0 ? _route.last : null;
  }

  get formattedDistance {
    if (_distance < 1000) {
      measure = 'M';
      return formatIntegers.format(_distance);
    }

    measure = 'KM';
    return formatDecimals.format(_distance / 1000);

  }

  get formattedTime {
    Duration diff = DateTime.now().difference(_startAt);

    String result = '';
    double hours = (diff.inSeconds / 60 / 60) % 60;
    result += padding(hours) + ':';

    double minutes = (diff.inSeconds / 60) % 60;
    result += padding(minutes) + ':';

    double seconds = (diff.inSeconds % 60).toDouble();
    result += padding(seconds);

    return result;
  }

  void addPoint(Map<String, dynamic> point) async {
    point['timestamp'] = DateTime.now();
    point['distance'] = 0;
    if (last != null) {
      double distanceInMeters = await Geolocator().distanceBetween(last['latitude'], last['longitude'], point['latitude'], point['longitude']);
      point['distance'] = distanceInMeters;
      _distance += distanceInMeters;
    }

    _route.add(point);
  }

  Future save() async {
    _endAt = DateTime.now();
    _totalTimeInSeconds = _endAt.difference(_startAt).inSeconds;
    await _uid.setData({
      'profile': _profile,
      'startAt': _startAt,
      'endAt': _endAt,
      'route': _route,
      'distance': _distance,
      'totalTimeInSeconds': _totalTimeInSeconds,
      'type': ActivityTypeArguments.type,
    });
  }

  String padding(double number) {
    String result = '';
    result += (number).toStringAsFixed(0);
    if (result.length < 2) {
      result = '0' + result;
    }

    return result;
  }
}

class ActivityPage extends StatefulWidget {
  static const String route = _route;

  @override
  State createState() => ActivityPageState();
}

class ActivityPageState extends State<ActivityPage> {
  double _distance = 0;
  bool _stopped = false;
  Route _route = Route(Profile.profile);
  String _time = '00:00:00';

  @override
  void initState() {
    _getPosition();
    _setTime();
    super.initState();
  }

  Future<bool> _stopActivity() async {
    switch (await showDialog<int>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding: EdgeInsets.all(8.0),
            title: const Text('Deseja finalizar a atividade?'),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () { Navigator.pop(context, 0); },
                child: Row(
                  children: <Widget>[
                    Icon(Icons.cancel),
                    Container(child: Text('Não, ainda não terminei'), padding: EdgeInsets.only(left: 10),),
                  ],
                ),
              ),

              SimpleDialogOption(
                onPressed: () { Navigator.pop(context, 1); },
                child: Row(
                  children: <Widget>[
                    Icon(Icons.check_circle),
                    Container(child: Text('Sim, finalizar atividade!'), padding: EdgeInsets.only(left: 10),),
                  ],
                ),
              ),
            ],
          );
        }
    )) {
      case 1:
        setState(() {
          _stopped = true;
        });
        await _route.save();

        return true;
        break;
      case 0:
        break;
    }

    return false;
  }

  void _setTime() async {
    if (_stopped == true) {
      return;
    }

    setState(() {
      _time = _route.formattedTime;
    });

    Future.delayed(Duration(milliseconds: 250), _setTime);
  }

  void _getPosition() async {
    if (_stopped == true) {
      return;
    }

    Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.best);

    if (_route.length == 0 || (_route.last['latitude'] != position.latitude && _route.last['longitude'] != position.longitude)) {
      _route.addPoint({
        'latitude': position.latitude,
        'longitude': position.longitude,
        'speed': position.speed,
      });
    }

    Future.delayed(Duration(milliseconds: 1500), _getPosition);
  }

  @override
  Widget build(BuildContext context) {
    final type = _activitiesTypes[ActivityTypeArguments.type];

    return WillPopScope(
      onWillPop: () => _stopActivity(),
      child: Scaffold(
        appBar: AppBar(title: Text(type)),
        body: Container(
          constraints: BoxConstraints.expand(),
          padding: EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text(_route.formattedDistance,  style: TextStyle(fontSize: 62, fontWeight: FontWeight.bold)),
                  Text(_route.measure, style: TextStyle(fontSize: 24)),
                ],
              ),

              Container(
                margin: EdgeInsets.only(top: 36, bottom: 36),
                child: Column(
                  children: <Widget>[
                    Text(_time,  style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold)),
                    Text('TEMPO', style: TextStyle(fontSize: 24)),
                  ],
                ),
              ),

              Container(
                width: 120,
                padding: EdgeInsets.all(16),
                child: RaisedButton(
                  padding: EdgeInsets.all(16),
                  color: Colors.red,
                  textColor: Colors.white,
                  child: Icon(Icons.stop, size: 48,),
                  shape: CircleBorder(),
                  onPressed: () async {
                    bool shouldStop = await _stopActivity();
                    if (shouldStop == true) {
                      Navigator.pop(context);
                    }
                  }
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
