import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:mobalt/colors.dart';
import 'package:mobalt/pages/activity.dart';
import 'package:mobalt/services/auth.dart';
import 'package:mobalt/services/profile.dart';

import '../widgets/drawer.dart';

const String _route = '/home';

class HomePage extends StatefulWidget {
  static const String route = _route;

  @override
  State createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  final Firestore db = Firestore.instance;
  final AuthService authService = AuthService();
  var _profile;
  var _action;

  HomePageState() {
    db.collection('/actions').where('active', isEqualTo: true).limit(1).snapshots().listen((querySnap) {
      querySnap.documents.forEach((actionSnap) {
        _action = actionSnap.data;
      });
    });

    var subs;
    subs = authService.auth.currentUser().asStream().listen((user) {
      if (authService.user != null) {
        subs.cancel();
        var _userUID = authService.user.uid;
        Profile.profile = db.document('/profiles/$_userUID');
        Profile.profile.snapshots().listen((profile) {
          if(!mounted) return;
          setState(() {
            _profile = profile.data;
            if (_profile['totalScore'] == null) {
              _profile['totalScore'] = 0;
            }
          });
        });
      }
    });
  }

  Future<void> _askActivityType() async {
    switch (await showDialog<int>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Selecione a atividade'),
          children: <Widget>[
            SimpleDialogOption(
              onPressed: () { Navigator.pop(context, 1); },
              child: Row(
                children: <Widget>[
                  Icon(Icons.directions_bike),
                  Container(child: Text('Bicicleta'), padding: EdgeInsets.only(left: 10),),
                ],
              ),
            ),
            SimpleDialogOption(
              onPressed: () { Navigator.pop(context, 0); },
              child: Row(
                children: <Widget>[
                  Icon(Icons.directions_walk),
                  Container(child: Text('A pé'), padding: EdgeInsets.only(left: 10),),
                ],
              ),
            ),
          ],
        );
      }
    )) {
      case 1:
        ActivityTypeArguments.type = 'bike';
        Navigator.pushNamed(context, ActivityPage.route);
        break;
      case 0:
        ActivityTypeArguments.type = 'on_foot';
        Navigator.pushNamed(context, ActivityPage.route);
        break;
    }
  }

  Widget _actionBuilder() {
    return Container(
      margin: EdgeInsets.only(bottom: 32.0),
      padding: EdgeInsets.all(16.0),
      decoration: BoxDecoration(color: yellowColor, borderRadius: BorderRadius.all(Radius.circular(8.0))),
      child: Row(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(right: 16.0),
            child: Icon(Icons.stars, size: 36, color: greyColor),
          ),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 8),
                  child: Text(_action['name'], style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold, color: greyColor),),
                ),
                Text(_action['description'], style: TextStyle(fontSize: 20, color: greyColor),),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _scoreBuilder() {
    return Text(_profile['totalScore'].toString(), style: TextStyle(fontSize: 48, fontWeight: FontWeight.bold),);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('MobAlt')),
      drawer: buildDrawer(context, _route),
      body: _profile != null ? Padding(
        padding: EdgeInsets.all(8.0),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              _action != null ? _actionBuilder() : Container(),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  _scoreBuilder(),
                  Text('pontos já acumulados', style: TextStyle(fontSize: 24),),
                ],
              )
            ]
          ),
        ),
      ) : Container(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          _askActivityType();
        },
        label: Text('Iniciar atividade'),
        icon: Icon(Icons.navigation),
      ),
    );
  }
}
