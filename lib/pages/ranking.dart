import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobalt/services/auth.dart';

const String _route = '/ranking';

class RankingPage extends StatefulWidget {
  static const String route = _route;

  @override
  State createState() => RankingPageState();
}

class RankingPageState extends State<RankingPage> {
  List _ranking = [];
//  bool _userInRanking = false;
  final Firestore db = Firestore.instance;
  final authService = AuthService();

  RankingPageState() {
//    authService.user

    db.collection('profiles').where('totalScore', isGreaterThan: 0).orderBy('totalScore', descending: true).limit(10).snapshots().listen((profiles) {
      var _tmpRank = [];
      profiles.documents.forEach((profile) {
        _tmpRank.add(profile.data);
      });
      
      setState(() {
        _ranking = _tmpRank;
      });
    });
  }
  
  Widget bodyBuilder(int index) {
    return Container(
      decoration: index > 0 ? BoxDecoration(border: Border(top: BorderSide(width: 1, color: Colors.grey))) : null,
      child: Row(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(12.0),
            child: Text((index + 1).toString(), style: TextStyle(fontSize: 28),),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(_ranking[index]['name'], style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
              Text(_ranking[index]['totalScore'].toString() + ' pontos', style: TextStyle(fontSize: 18, color: Colors.grey),),
            ],
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Ranking')),
      body: Container(
        padding: EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Nossos usuários com mais pontos', style: TextStyle(fontSize: 20),),
            Expanded(
              child: ListView.builder(
                padding: EdgeInsets.all(8.0),
                itemCount: _ranking.length,
                itemBuilder: (BuildContext context, int index) => bodyBuilder(index),
              ),
            )
          ],
        ),
      )
    );
  }
}