import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:mobalt/pages/ranking.dart';
import 'package:mobalt/pages/register.dart';
import 'package:mobalt/pages/home.dart';
import 'package:mobalt/services/errors.dart';

class AuthPage extends StatelessWidget {
  static const String route = '/auth';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('MobAlt'),
        centerTitle: true,
        actions: <Widget>[
          FlatButton(
            textColor: Colors.white,
            child: Icon(Icons.trending_up),
            onPressed: () {
              Navigator.pushNamed(context, RankingPage.route);
            },
          )
        ],
      ),
      body: ListView(
        padding: EdgeInsets.only(left: 8.0, right: 8, top: 8, bottom: 0),
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 16),
            child: Text('Entre com o e-mail e senha de sua conta:', style: TextStyle(fontSize: 18),),
            alignment: Alignment.center,
          ),
          Container(
            child: LoginForm(),
          ),
          Container(
            padding: EdgeInsets.only(top: 16),
            alignment: Alignment.center,
            child: Column(
              children: <Widget>[
                Text('Não tem uma conta?', style: TextStyle(fontSize: 18),),
                FlatButton(
                  child: Text('CRIAR UMA CONTA', style: TextStyle(color: Colors.blue)),
                  onPressed: () {
                    Navigator.pushNamed(context, RegisterPage.route);
                  },
                ),
              ],
            )
          ),
        ],
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  @override
  State createState() => LoginFormState();
}

class LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Firestore db = Firestore.instance;

  String _errorMessage;
  bool _processing = false;

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  void formSubmit() {
    if (emailController.text.isEmpty || passwordController.text.isEmpty) {
      return;
    }

    setState(() {
      _errorMessage = null;
      _processing = true;
    });

    String email = emailController.text;
    String pass = passwordController.text;

    void redirect() {
      setState(() {
        _processing = false;
      });

      emailController.clear();
      passwordController.clear();
      Navigator.pushReplacementNamed(context, HomePage.route);
    }

    _auth.signInWithEmailAndPassword(email: email, password: pass)
      .then((AuthResult result) {
        final String _userUID = result.user.uid;
        var subs;
        subs = db.document('/profiles/$_userUID').snapshots().listen((profile) {
          var data = profile.data;
          subs.cancel();
          if (data == null) {
            db.document('/profiles/$_userUID').setData({
              'uid': _userUID,
              'name': '',
              'email': result.user.email,
            }, merge: true).whenComplete(redirect);
          } else {
            redirect();
          }
        });
      })
      .catchError((error) {
        print(error);
        setState(() {
          _processing = false;
          _errorMessage = errors[error.code];
        });
      });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          TextFormField(
            controller: emailController,
            onEditingComplete: formSubmit,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(labelText: 'E-mail'),
            validator: (value) {
              if (value.isEmpty) {
                return 'Insira seu e-mail';
              }
              return null;
            },
          ),
          TextFormField(
            controller: passwordController,
            onEditingComplete: formSubmit,
            keyboardType: TextInputType.text,
            obscureText: true,
            decoration: InputDecoration(labelText: 'Senha'),
            validator: (value) {
              if (value.isEmpty) {
                return 'Insira sua senha';
              }
              return null;
            },
          ),

          _errorMessage == null ? Container() : Column(
            children: <Widget>[
              Text('Houve um erro:', style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),),
              Text(_errorMessage, style: TextStyle(color: Colors.red)),
            ],
          ),

          Container(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Container(
              constraints: BoxConstraints.expand(height: 36),
              child: FlatButton(
                color: Colors.blue,
                textColor: Colors.white,
                onPressed: _processing == true ? null : () {
                  if (_formKey.currentState.validate()) {
                    formSubmit();
                  }
                },
                child: Text('ENTRAR'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}