import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong/latlong.dart';
import 'package:mobalt/services/profile.dart';
import 'package:intl/intl.dart';

const String _route = '/activity';

const Map _activitiesTypes = {
  'bike': 'Bicicleta',
  'on_foot': 'A pé',
};

class ActivityTypeArguments {
  static String type;
}

class Route {
  DocumentReference _uid;
  DocumentReference _profile;
  DateTime _startAt;
  DateTime _endAt;
  List<Map<String, dynamic>> _route;
  double _distance = 0;
  int _totalTimeInSeconds;

  final Firestore db = Firestore.instance;
  final formatDecimals = new NumberFormat('###.00', 'pt_BR');
  final formatIntegers = new NumberFormat('###', 'pt_BR');

  Route(DocumentReference profile) {
    db.settings(persistenceEnabled: true);
    _uid = db.collection('/routes').document();
    _profile = profile;
    _route = List();
    _startAt = new DateTime.now();
  }

  get length {
    return _route.length;
  }

  get last {
    return _route.length > 0 ? _route.last : null;
  }

  get formattedDistance {
    if (_distance < 1000) {
      return formatIntegers.format(_distance) + ' m';
    }

    return formatDecimals.format(_distance / 1000) + ' km';
  }

  get formattedTime {
    Duration diff = DateTime.now().difference(_startAt);

    String result = '';
    double hours = (diff.inSeconds / 60 / 60) % 60;
    if (hours >= 1) {
      result += (hours).toStringAsFixed(0) + 'h ';
    }

    double minutes = (diff.inSeconds / 60) % 60;
    if (minutes >= 1) {
      result += (minutes).toStringAsFixed(0) + 'min ';
    }

    int seconds = diff.inSeconds % 60;
    if (seconds < 10) {
      result += '0';
    }
    result += seconds.toString() + 's';

    return result;
  }

  void addPoint(Map<String, dynamic> point) async {
    point['timestamp'] = DateTime.now();
    point['distance'] = 0;
    if (last != null) {
      double distanceInMeters = await Geolocator().distanceBetween(last['latitude'], last['longitude'], point['latitude'], point['longitude']);
      point['distance'] = distanceInMeters;
      _distance += distanceInMeters;
    }

    _route.add(point);
  }

  Future save() async {
    _endAt = DateTime.now();
    _totalTimeInSeconds = _endAt.difference(_startAt).inSeconds;
    await _uid.setData({
      'profile': _profile,
      'startAt': _startAt,
      'endAt': _endAt,
      'route': _route,
      'distance': _distance,
      'totalTimeInSeconds': _totalTimeInSeconds,
      'type': ActivityTypeArguments.type,
    });
  }
}

class ActivityPage extends StatefulWidget {
  static const String route = _route;

  @override
  State createState() => ActivityPageState();
}

class ActivityPageState extends State<ActivityPage> {
  double _posX = 0;
  double _posY = 0;
//  double _speed = 0;
  double _zoom = 17.0;
  double _distance = 0;
  bool _stopped = false;
  bool _isCentered = true;
  LatLng _center = LatLng(-26.2861838, -48.9949691);
  MapController _mapController;
  Route _route = Route(Profile.profile);
  String _time = '0s';

  @override
  void initState() {
    _getPosition();
    _setTime();
    _mapController = MapController();
    super.initState();
  }

  Future<bool> _stopActivity() async {
    switch (await showDialog<int>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text('Deseja parar a atividade?'),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () { Navigator.pop(context, 1); },
                child: Row(
                  children: <Widget>[
                    Icon(Icons.check_circle),
                    Container(child: Text('Sim!'), padding: EdgeInsets.only(left: 10),),
                  ],
                ),
              ),
              SimpleDialogOption(
                onPressed: () { Navigator.pop(context, 0); },
                child: Row(
                  children: <Widget>[
                    Icon(Icons.cancel),
                    Container(child: Text('Não, quero continuar.'), padding: EdgeInsets.only(left: 10),),
                  ],
                ),
              ),
            ],
          );
        }
    )) {
      case 1:
        setState(() {
          _stopped = true;
        });
        await _route.save();

        return true;
        break;
      case 0:
        break;
    }

    return false;
  }

  void _setTime() async {
    if (_stopped == true) {
      return;
    }

    setState(() {
      _time = _route.formattedTime;
    });

    Future.delayed(Duration(milliseconds: 250), _setTime);
  }

  void _getPosition() async {
    if (_stopped == true) {
      return;
    }

    Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.best);

    if (_route.length == 0 || (_route.last['latitude'] != position.latitude && _route.last['longitude'] != position.longitude)) {
      _route.addPoint({
        'latitude': position.latitude,
        'longitude': position.longitude,
        'speed': position.speed,
      });

      setState(() {
        _posX = position.latitude;
        _posY = position.longitude;

        if (_isCentered == true) {
          _center = LatLng(_posX, _posY);
          _mapController.move(_center, _zoom);
        }
      });
    }

    Future.delayed(Duration(milliseconds: 250), _getPosition);
  }

  @override
  Widget build(BuildContext context) {
    final type = _activitiesTypes[ActivityTypeArguments.type];

    return WillPopScope(
      onWillPop: () => _stopActivity(),
      child: Scaffold(
        appBar: AppBar(title: Text(type)),
        body: Container(
          constraints: BoxConstraints.expand(),
          child: Column(
            children: [
              Flexible(
                flex: 2,
                child: FlutterMap(
                  mapController: _mapController,
                  options: new MapOptions(
                    center: _center,
                    zoom: _zoom,
                  ),
                  layers: [
                    new TileLayerOptions(
                      urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                      subdomains: ['a', 'b', 'c'],
                      additionalOptions: {
                        'attribution': 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
                      },
                    ),
                    new MarkerLayerOptions(
                      markers: [
                        new Marker(
                          width: 16.0,
                          height: 16.0,
                          point: _center,
                          builder: (ctx) =>
                          new Container(
                            width: 16,
                            height: 16,
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.all(Radius.circular(8.0),),
                            )
                          ),
                        ),
                      ],
                    ),
                  ]
                )
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: 120,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Text('Tempo'),
                        Text(_time, style: TextStyle(fontSize: 18)),
                      ],
                    ),
                  ),

                  Container(
                    width: 120,
                    padding: EdgeInsets.all(8),
                    child: RaisedButton(
                      padding: EdgeInsets.all(8),
                      color: Colors.red,
                      textColor: Colors.white,
                      child: Icon(Icons.stop, size: 32),
                      shape: CircleBorder(),
                      onPressed: () async {
                        bool shouldStop = await _stopActivity();
                        if (shouldStop == true) {
                          Navigator.pop(context);
                        }
                      }
                    ),
                  ),

                  Container(
                    width: 120,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Distância'),
                        Text(_route.formattedDistance, style: TextStyle(fontSize: 18)),
                      ],
                    ),
                  ),
                ]
              )
            ],
          )
        )
      ),
    );
  }
}
